# Zeri3a typeface

Typeface for artists Salim Bayri and Ghita Skali, for their project ["Large Quantities of Sunflower Seeds are for Birds... and sometimes People"](https://www.de-ateliers.nl/event/ghita-skali-salim-bayri-large-quantities-of-sunflower-seeds-are-for-animals-and-sometimes-people/).

[DOWNLOAD LINK](https://gitlab.com/tbayri/zerri3a/raw/master/dist/Zerri3a.ttf?inline=false)

### Tips

If you draw letters on Illustrator, use the provided script (`./tools/MultiExporter.jsx`) to export all layers to svg.


